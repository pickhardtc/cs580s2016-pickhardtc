#Claire Pickhardt
#Computer Science 580
#8 March 2016
#Python graph example
#Honor Code: The work that I am submitting is the result of my own thinking and efforts.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

txtfile = np.genfromtxt("AirPassengers.csv", delimiter=',',unpack=True, names=['x','y'])
plt.plot(txtfile['x'], txtfile['y'], color = 'g', label='text from csv file')

plt.title("Airline Passenger Numbers")
plt.ylabel("Year")
plt.xlabel("Number of Passengers")
plt.grid(True)
plt.show()
